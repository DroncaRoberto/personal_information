package com.company;

import java.io.*;
import java.util.ArrayList;

public class Main {


    public static void main(String[] args) {
        for( Person person : createPerson(getData())){
            System.out.println(person);
        }
    }

    public static ArrayList<Person> createPerson(String data) {
        String firstName = " ";
        String lastName = " ";
        String phoneNumber = " ";
        String[] personsList = data.split(";");
        String [] person ;
        ArrayList<Person> personArrayList = new ArrayList<>();
        Person person1 = null;

        for (String s : personsList) {
            person = s.split(",");
            for (int j = 0; j < person.length; j++) {
                if (j == 0) {
                    firstName = person[0];
                } else if (j == 1) {
                    lastName = person[1];
                } else if (j == 2) {
                    phoneNumber = (person[2]);
                }
                person1 = new Person(firstName, lastName, phoneNumber);
            }
            personArrayList.add(person1);
        }
        return personArrayList ;
    }

    public static String getData(){
        String result = "";
        String partialResult = "";
        try {
            FileReader file = new FileReader("PersonalData.txt");
            BufferedReader bufferedReader = new BufferedReader(file);
            while ((partialResult = bufferedReader.readLine()) != null){
                result += partialResult;
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
